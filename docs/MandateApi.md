# swagger_client.MandateApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_mandate**](MandateApi.md#delete_mandate) | **DELETE** /mandates/{id} | revoke a mandate
[**get_mandate**](MandateApi.md#get_mandate) | **GET** /mandates/{id} | get mandate
[**get_mandates**](MandateApi.md#get_mandates) | **GET** /mandates | search mandates
[**mandates_id_resend_otp_put**](MandateApi.md#mandates_id_resend_otp_put) | **PUT** /mandates/{id}/ResendOtp/ | send an OTP
[**mandates_id_sign_put**](MandateApi.md#mandates_id_sign_put) | **PUT** /mandates/{id}/Sign/ | sign a mandate
[**post_mandates**](MandateApi.md#post_mandates) | **POST** /mandates | create a mandate


# **delete_mandate**
> object delete_mandate(id, origin)

revoke a mandate

Change mandate's status to canceled.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.MandateApi(swagger_client.ApiClient(configuration))
id = 56 # int | Mandate's internal id.
origin = 'origin_example' # str | The origin of the request for revocation

try:
    # revoke a mandate
    api_response = api_instance.delete_mandate(id, origin)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MandateApi->delete_mandate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Mandate&#39;s internal id. | 
 **origin** | **str**| The origin of the request for revocation | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_mandate**
> object get_mandate(id)

get mandate

Get a mandate from the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.MandateApi(swagger_client.ApiClient(configuration))
id = 789 # int | Mandate's internal id.

try:
    # get mandate
    api_response = api_instance.get_mandate(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MandateApi->get_mandate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Mandate&#39;s internal id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_mandates**
> object get_mandates(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, mandate_id=mandate_id, user_id=user_id, unique_mandate_reference=unique_mandate_reference, mandate_status=mandate_status, filter=filter)

search mandates

Get mandates that match search criteria.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.MandateApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
mandate_id = 56 # int | Mandate's Id (optional)
user_id = 56 # int | User's id who initiates the mandate request. (optional)
unique_mandate_reference = 'unique_mandate_reference_example' # str | Unique Mandate Reference (UMR) of the searched mandate. It is a sequence of characters that ensures the mandate traceability. UMR coupled with SCI allows to uniquely identify a creditor and a contract for any mandate. It is provided at the mandate creation. (optional)
mandate_status = 'mandate_status_example' # str | Mandate's status. Possible values: * CANCELED * PENDING * VALIDATED  (optional)
filter = 'filter_example' # str | You can filter the API response by using filter(s). Filters should be separated by a \";\". Example for 3 filters : FILTER1;FILTER2;FILTER3. A single filter has the following syntax : \"fieldName criterion expression\". Where :  - fieldName : the name of a filterable field of this object.   - expression : the values to be included or excluded (see the table below for more information)   - criterion : a filter criterion.  Here are the possible values for criterion :   | Criteria |         Description    |                   Type                   | Expression Example |  |----------|------------------------|------------------------------------------|--------------------|  |     >    |      greater than      |            alphanumeric string           |         100        |  |    >=    | greater or equal than  |            alphanumeric string           |         100        |  |     <    |        less than       |            alphanumeric string           |         100        |  |    <=    |   less or equal than   |            alphanumeric string           |         100        |  |    !=    |      not equal to      |            alphanumeric string           |         100        |  |   like   |          like          |            alphanumeric string           |         100        |  |    in    |           in           | alphanumeric strings separated by commas |      100,30,25     |  |    ==    |         equals         |            alphanumeric string           |         100        |  (optional)

try:
    # search mandates
    api_response = api_instance.get_mandates(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, mandate_id=mandate_id, user_id=user_id, unique_mandate_reference=unique_mandate_reference, mandate_status=mandate_status, filter=filter)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MandateApi->get_mandates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **mandate_id** | **int**| Mandate&#39;s Id | [optional] 
 **user_id** | **int**| User&#39;s id who initiates the mandate request. | [optional] 
 **unique_mandate_reference** | **str**| Unique Mandate Reference (UMR) of the searched mandate. It is a sequence of characters that ensures the mandate traceability. UMR coupled with SCI allows to uniquely identify a creditor and a contract for any mandate. It is provided at the mandate creation. | [optional] 
 **mandate_status** | **str**| Mandate&#39;s status. Possible values: * CANCELED * PENDING * VALIDATED  | [optional] 
 **filter** | **str**| You can filter the API response by using filter(s). Filters should be separated by a \&quot;;\&quot;. Example for 3 filters : FILTER1;FILTER2;FILTER3. A single filter has the following syntax : \&quot;fieldName criterion expression\&quot;. Where :  - fieldName : the name of a filterable field of this object.   - expression : the values to be included or excluded (see the table below for more information)   - criterion : a filter criterion.  Here are the possible values for criterion :   | Criteria |         Description    |                   Type                   | Expression Example |  |----------|------------------------|------------------------------------------|--------------------|  |     &gt;    |      greater than      |            alphanumeric string           |         100        |  |    &gt;&#x3D;    | greater or equal than  |            alphanumeric string           |         100        |  |     &lt;    |        less than       |            alphanumeric string           |         100        |  |    &lt;&#x3D;    |   less or equal than   |            alphanumeric string           |         100        |  |    !&#x3D;    |      not equal to      |            alphanumeric string           |         100        |  |   like   |          like          |            alphanumeric string           |         100        |  |    in    |           in           | alphanumeric strings separated by commas |      100,30,25     |  |    &#x3D;&#x3D;    |         equals         |            alphanumeric string           |         100        |  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mandates_id_resend_otp_put**
> object mandates_id_resend_otp_put(id, access_token, access_signature, user_id, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, debtor_mobile=debtor_mobile)

send an OTP

Send an OTP to sign a specific mandate. The OTP will be sent to the user's mobile phone OR to a specific mobile phone number in the request. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.MandateApi(swagger_client.ApiClient(configuration))
id = 56 # int | Mandate's id.
access_token = 'access_token_example' # str | Access token must be defined here or in Authorization HTTP header. More info [here](https://agent.treezor.com/security-authentication). 
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication). 
user_id = 56 # int | User's id who initiates the mandate request.
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
debtor_mobile = 'debtor_mobile_example' # str | Debtor's mobile phone number. Used to send the OTP for signature (optional)

try:
    # send an OTP
    api_response = api_instance.mandates_id_resend_otp_put(id, access_token, access_signature, user_id, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, debtor_mobile=debtor_mobile)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MandateApi->mandates_id_resend_otp_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Mandate&#39;s id. | 
 **access_token** | **str**| Access token must be defined here or in Authorization HTTP header. More info [here](https://agent.treezor.com/security-authentication).  | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | 
 **user_id** | **int**| User&#39;s id who initiates the mandate request. | 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **debtor_mobile** | **str**| Debtor&#39;s mobile phone number. Used to send the OTP for signature | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mandates_id_sign_put**
> object mandates_id_sign_put(id, access_token, access_signature, user_id, otp, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, debtor_signature_ip=debtor_signature_ip)

sign a mandate

Sign a mandate with the received OTP.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.MandateApi(swagger_client.ApiClient(configuration))
id = 56 # int | Mandate's id.
access_token = 'access_token_example' # str | Access token must be defined here or in Authorization HTTP header. More info [here](https://agent.treezor.com/security-authentication). 
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication). 
user_id = 56 # int | User's is who initiates the mandate request.
otp = 'otp_example' # str | The One-time password that the user got once mandate created.
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
debtor_signature_ip = 'debtor_signature_ip_example' # str | IP address from which the mandate will be signed. (optional)

try:
    # sign a mandate
    api_response = api_instance.mandates_id_sign_put(id, access_token, access_signature, user_id, otp, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, debtor_signature_ip=debtor_signature_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MandateApi->mandates_id_sign_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Mandate&#39;s id. | 
 **access_token** | **str**| Access token must be defined here or in Authorization HTTP header. More info [here](https://agent.treezor.com/security-authentication).  | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | 
 **user_id** | **int**| User&#39;s is who initiates the mandate request. | 
 **otp** | **str**| The One-time password that the user got once mandate created. | 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **debtor_signature_ip** | **str**| IP address from which the mandate will be signed. | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_mandates**
> object post_mandates(sdd_type, is_paper, user_id, debtor_name, debtor_address, debtor_city, debtor_zip_code, debtor_country, debtor_iban, sequence_type, signature_date, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, debtor_bic=debtor_bic, created_ip=created_ip)

create a mandate

Create a new mandate in the system. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.MandateApi(swagger_client.ApiClient(configuration))
sdd_type = 'sdd_type_example' # str | Type of mandate.  | Type | Description | | ---- | ----------- | | core | Sepa Direct Debit Core | | b2b | Sepa Direct Debit Business 2 Business  | 
is_paper = true # bool | Indicate if it's a paper mandate or not
user_id = 'user_id_example' # str | User's id who initiates the mandate request. The creditor.
debtor_name = 'debtor_name_example' # str | Debtor's full name. Must contain at least 3 alphabetical characters.
debtor_address = 'debtor_address_example' # str | Debtor's address.
debtor_city = 'debtor_city_example' # str | Debtor's city.
debtor_zip_code = 'debtor_zip_code_example' # str | Debtor's zip code
debtor_country = 'debtor_country_example' # str | Debtor's country
debtor_iban = 'debtor_iban_example' # str | Debtor's IBAN
sequence_type = 'sequence_type_example' # str | Type of mandate.  | Type | Description | | ---- | ----------- | | one-off | One-off payment | | recurrent | Recurrent payment |  Default value: one-off. 
signature_date = 'signature_date_example' # str | Signature date of the mandate. Required if is paper.
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
debtor_bic = 'debtor_bic_example' # str | Debtor's BIC (or SWIFT) (optional)
created_ip = 'created_ip_example' # str | IP address of the system that created mandate. (optional)

try:
    # create a mandate
    api_response = api_instance.post_mandates(sdd_type, is_paper, user_id, debtor_name, debtor_address, debtor_city, debtor_zip_code, debtor_country, debtor_iban, sequence_type, signature_date, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, debtor_bic=debtor_bic, created_ip=created_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MandateApi->post_mandates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sdd_type** | **str**| Type of mandate.  | Type | Description | | ---- | ----------- | | core | Sepa Direct Debit Core | | b2b | Sepa Direct Debit Business 2 Business  |  | 
 **is_paper** | **bool**| Indicate if it&#39;s a paper mandate or not | 
 **user_id** | **str**| User&#39;s id who initiates the mandate request. The creditor. | 
 **debtor_name** | **str**| Debtor&#39;s full name. Must contain at least 3 alphabetical characters. | 
 **debtor_address** | **str**| Debtor&#39;s address. | 
 **debtor_city** | **str**| Debtor&#39;s city. | 
 **debtor_zip_code** | **str**| Debtor&#39;s zip code | 
 **debtor_country** | **str**| Debtor&#39;s country | 
 **debtor_iban** | **str**| Debtor&#39;s IBAN | 
 **sequence_type** | **str**| Type of mandate.  | Type | Description | | ---- | ----------- | | one-off | One-off payment | | recurrent | Recurrent payment |  Default value: one-off.  | 
 **signature_date** | **str**| Signature date of the mandate. Required if is paper. | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **debtor_bic** | **str**| Debtor&#39;s BIC (or SWIFT) | [optional] 
 **created_ip** | **str**| IP address of the system that created mandate. | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

