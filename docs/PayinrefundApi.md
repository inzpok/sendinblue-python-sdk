# swagger_client.PayinrefundApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_payinrefund**](PayinrefundApi.md#get_payinrefund) | **GET** /payinrefunds/{id} | get a pay in refund
[**get_payinrefunds**](PayinrefundApi.md#get_payinrefunds) | **GET** /payinrefunds | search pay in refunds


# **get_payinrefund**
> object get_payinrefund(id)

get a pay in refund

Get a payin refund from the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PayinrefundApi(swagger_client.ApiClient(configuration))
id = 789 # int | Payinrefund's internal id.

try:
    # get a pay in refund
    api_response = api_instance.get_payinrefund(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PayinrefundApi->get_payinrefund: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Payinrefund&#39;s internal id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_payinrefunds**
> object get_payinrefunds(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, payin_id=payin_id, payinrefund_id=payinrefund_id, payinrefund_tag=payinrefund_tag, payinrefund_status=payinrefund_status, wallet_id=wallet_id, payinrefund_date=payinrefund_date, user_id=user_id, amount=amount, currency=currency, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to)

search pay in refunds

Search for payinrefunds in the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PayinrefundApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
payin_id = 56 # int | Payin's id. (optional)
payinrefund_id = 56 # int | Payinrefund's id. (optional)
payinrefund_tag = 'payinrefund_tag_example' # str | Custom data. (optional)
payinrefund_status = 'payinrefund_status_example' # str | Payinrefund's status. Possible values: * PENDING * CANCELED * VALIDATED (optional)
wallet_id = 56 # int | Wallet's id to refund. (optional)
payinrefund_date = '2013-10-20T19:20:30+01:00' # datetime | payinrefund's date. Format: YYYY-MM-DD (optional)
user_id = 56 # int | User's id who made refunds. (optional)
amount = 'amount_example' # str | Refund's amount. (optional)
currency = 'currency_example' # str | Transfert amount currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  (optional)
page_number = 56 # int | Pagination page number. More info [here](https://agent.treezor.com/lists).  (optional)
page_count = 56 # int | The number of items per page. More info [here](https://agent.treezor.com/lists).  (optional)
sort_by = 'sort_by_example' # str | The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  (optional)
sort_order = 'sort_order_example' # str | The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  (optional)
created_date_from = '2013-10-20T19:20:30+01:00' # datetime | The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
created_date_to = '2013-10-20T19:20:30+01:00' # datetime | The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_from = '2013-10-20T19:20:30+01:00' # datetime | The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_to = '2013-10-20T19:20:30+01:00' # datetime | The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)

try:
    # search pay in refunds
    api_response = api_instance.get_payinrefunds(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, payin_id=payin_id, payinrefund_id=payinrefund_id, payinrefund_tag=payinrefund_tag, payinrefund_status=payinrefund_status, wallet_id=wallet_id, payinrefund_date=payinrefund_date, user_id=user_id, amount=amount, currency=currency, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PayinrefundApi->get_payinrefunds: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **payin_id** | **int**| Payin&#39;s id. | [optional] 
 **payinrefund_id** | **int**| Payinrefund&#39;s id. | [optional] 
 **payinrefund_tag** | **str**| Custom data. | [optional] 
 **payinrefund_status** | **str**| Payinrefund&#39;s status. Possible values: * PENDING * CANCELED * VALIDATED | [optional] 
 **wallet_id** | **int**| Wallet&#39;s id to refund. | [optional] 
 **payinrefund_date** | **datetime**| payinrefund&#39;s date. Format: YYYY-MM-DD | [optional] 
 **user_id** | **int**| User&#39;s id who made refunds. | [optional] 
 **amount** | **str**| Refund&#39;s amount. | [optional] 
 **currency** | **str**| Transfert amount currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  | [optional] 
 **page_number** | **int**| Pagination page number. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **page_count** | **int**| The number of items per page. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_by** | **str**| The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_order** | **str**| The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **created_date_from** | **datetime**| The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **created_date_to** | **datetime**| The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_from** | **datetime**| The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_to** | **datetime**| The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

