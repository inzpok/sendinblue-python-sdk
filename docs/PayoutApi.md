# swagger_client.PayoutApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_payout**](PayoutApi.md#delete_payout) | **DELETE** /payouts/{id} | cancel a payout
[**get_payout**](PayoutApi.md#get_payout) | **GET** /payouts/{id} | get a payout
[**get_payouts**](PayoutApi.md#get_payouts) | **GET** /payouts | search payout(s)
[**post_payout**](PayoutApi.md#post_payout) | **POST** /payouts | create a pay out


# **delete_payout**
> object delete_payout(id)

cancel a payout

Change pay out status to CANCELED. A VALIDATED pay out can't be canceled.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PayoutApi(swagger_client.ApiClient(configuration))
id = 789 # int | Payouts internal id.

try:
    # cancel a payout
    api_response = api_instance.delete_payout(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PayoutApi->delete_payout: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Payouts internal id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_payout**
> object get_payout(id)

get a payout

Get a pay out from the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PayoutApi(swagger_client.ApiClient(configuration))
id = 789 # int | Payouts internal id.

try:
    # get a payout
    api_response = api_instance.get_payout(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PayoutApi->get_payout: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Payouts internal id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_payouts**
> object get_payouts(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, payout_id=payout_id, payout_tag=payout_tag, payout_status=payout_status, payout_type_id=payout_type_id, wallet_id=wallet_id, user_id=user_id, payout_date=payout_date, bankaccount_id=bankaccount_id, beneficiary_id=beneficiary_id, amount=amount, currency=currency, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to)

search payout(s)

Get payout(s) that match search criteria. The request must contains at least one of those inputs  payoutId, bankaccountId, beneficiaryId, payoutTag

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PayoutApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
payout_id = 56 # int | Payout's Id (optional)
payout_tag = 'payout_tag_example' # str | Custom data. (optional)
payout_status = 'payout_status_example' # str | pay out status; Possible values: * CANCELED * PENDING * VALIDATED  (optional)
payout_type_id = 56 # int | Pay out type's id :  | ID | Description | |-----|-----| | 1 | Credit Transfer | | 2 | Direct Debit |  (optional)
wallet_id = 56 # int | Pay out wallet's id. (optional)
user_id = 56 # int | Pay out user's id. (optional)
payout_date = '2013-10-20T19:20:30+01:00' # datetime | pay out execution date. Format: YYYY-MM-DD HH:MM:SS'  (optional)
bankaccount_id = 56 # int | Pay out bank account id. (optional)
beneficiary_id = 56 # int | Pay out beneficiary id. (optional)
amount = 'amount_example' # str | Pay out amount. (optional)
currency = 'currency_example' # str | Transfert amount currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  (optional)
page_number = 56 # int | Pagination page number. More info [here](https://agent.treezor.com/lists).  (optional)
page_count = 56 # int | The number of items per page. More info [here](https://agent.treezor.com/lists).  (optional)
sort_by = 'sort_by_example' # str | The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  (optional)
sort_order = 'sort_order_example' # str | The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  (optional)
created_date_from = '2013-10-20T19:20:30+01:00' # datetime | The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
created_date_to = '2013-10-20T19:20:30+01:00' # datetime | The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_from = '2013-10-20T19:20:30+01:00' # datetime | The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_to = '2013-10-20T19:20:30+01:00' # datetime | The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)

try:
    # search payout(s)
    api_response = api_instance.get_payouts(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, payout_id=payout_id, payout_tag=payout_tag, payout_status=payout_status, payout_type_id=payout_type_id, wallet_id=wallet_id, user_id=user_id, payout_date=payout_date, bankaccount_id=bankaccount_id, beneficiary_id=beneficiary_id, amount=amount, currency=currency, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PayoutApi->get_payouts: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **payout_id** | **int**| Payout&#39;s Id | [optional] 
 **payout_tag** | **str**| Custom data. | [optional] 
 **payout_status** | **str**| pay out status; Possible values: * CANCELED * PENDING * VALIDATED  | [optional] 
 **payout_type_id** | **int**| Pay out type&#39;s id :  | ID | Description | |-----|-----| | 1 | Credit Transfer | | 2 | Direct Debit |  | [optional] 
 **wallet_id** | **int**| Pay out wallet&#39;s id. | [optional] 
 **user_id** | **int**| Pay out user&#39;s id. | [optional] 
 **payout_date** | **datetime**| pay out execution date. Format: YYYY-MM-DD HH:MM:SS&#39;  | [optional] 
 **bankaccount_id** | **int**| Pay out bank account id. | [optional] 
 **beneficiary_id** | **int**| Pay out beneficiary id. | [optional] 
 **amount** | **str**| Pay out amount. | [optional] 
 **currency** | **str**| Transfert amount currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  | [optional] 
 **page_number** | **int**| Pagination page number. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **page_count** | **int**| The number of items per page. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_by** | **str**| The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_order** | **str**| The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **created_date_from** | **datetime**| The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **created_date_to** | **datetime**| The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_from** | **datetime**| The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_to** | **datetime**| The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_payout**
> object post_payout(wallet_id, amount, currency, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, payout_tag=payout_tag, bankaccount_id=bankaccount_id, beneficiary_id=beneficiary_id, label=label, supporting_file_link=supporting_file_link)

create a pay out

Create a new pay out in the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PayoutApi(swagger_client.ApiClient(configuration))
wallet_id = 56 # int | Pay out wallet id.
amount = 3.4 # float | Pay out amount.
currency = 'currency_example' # str | Transfert amount currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217). 
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
payout_tag = 'payout_tag_example' # str | Custom data. (optional)
bankaccount_id = 56 # int | Pay out bank account's id. Mandatory id beneficiaryId is not provided. If bankaccountId and beneficiaryId are both provided, the beneficiaryId will be used. (optional)
beneficiary_id = 56 # int | Pay out beneficiary's id. Mandatory id bankaccountId is not provided. If bankaccountId and beneficiaryId are both provided, the beneficiaryId will be used. (optional)
label = 'label_example' # str | Pay out label that will be displayed in the receiver's account (140 chars max). (optional)
supporting_file_link = 'supporting_file_link_example' # str | Support file link (optional)

try:
    # create a pay out
    api_response = api_instance.post_payout(wallet_id, amount, currency, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, payout_tag=payout_tag, bankaccount_id=bankaccount_id, beneficiary_id=beneficiary_id, label=label, supporting_file_link=supporting_file_link)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PayoutApi->post_payout: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wallet_id** | **int**| Pay out wallet id. | 
 **amount** | **float**| Pay out amount. | 
 **currency** | **str**| Transfert amount currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **payout_tag** | **str**| Custom data. | [optional] 
 **bankaccount_id** | **int**| Pay out bank account&#39;s id. Mandatory id beneficiaryId is not provided. If bankaccountId and beneficiaryId are both provided, the beneficiaryId will be used. | [optional] 
 **beneficiary_id** | **int**| Pay out beneficiary&#39;s id. Mandatory id bankaccountId is not provided. If bankaccountId and beneficiaryId are both provided, the beneficiaryId will be used. | [optional] 
 **label** | **str**| Pay out label that will be displayed in the receiver&#39;s account (140 chars max). | [optional] 
 **supporting_file_link** | **str**| Support file link | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

