# swagger_client.RecallRApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_recall_r**](RecallRApi.md#get_recall_r) | **GET** /recallRs | get recalls or RRO
[**get_recall_r_id**](RecallRApi.md#get_recall_r_id) | **GET** /recallRs/{id} | get recalls or RRO
[**put_recall_r**](RecallRApi.md#put_recall_r) | **PUT** /recallRs/{id}/response/ | Consent on a recall or RRO


# **get_recall_r**
> object get_recall_r(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, page_count=page_count, page_number=page_number, filter=filter)

get recalls or RRO

get recalls or RRO

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.RecallRApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
page_count = 56 # int | Maximum number of elements in on call. By default 50.  (optional)
page_number = 56 # int | Page of asked resulset. By default it is the first page.  (optional)
filter = 'filter_example' # str | List of available filters: - id - recallRSepaMessageId - cxlId - statusId - reasonCode - clientId - userId - walletId - sctrId - receivedDate - frozenWalletFollowingRecallr - createdDate - inError  (optional)

try:
    # get recalls or RRO
    api_response = api_instance.get_recall_r(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, page_count=page_count, page_number=page_number, filter=filter)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RecallRApi->get_recall_r: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **page_count** | **int**| Maximum number of elements in on call. By default 50.  | [optional] 
 **page_number** | **int**| Page of asked resulset. By default it is the first page.  | [optional] 
 **filter** | **str**| List of available filters: - id - recallRSepaMessageId - cxlId - statusId - reasonCode - clientId - userId - walletId - sctrId - receivedDate - frozenWalletFollowingRecallr - createdDate - inError  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_recall_r_id**
> object get_recall_r_id(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, page_count=page_count, page_number=page_number)

get recalls or RRO

get recalls or RRO

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.RecallRApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
page_count = 56 # int | Maximum number of elements in on call. By default 50.  (optional)
page_number = 56 # int | Page of asked resulset. By default it is the first page.  (optional)

try:
    # get recalls or RRO
    api_response = api_instance.get_recall_r_id(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, page_count=page_count, page_number=page_number)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RecallRApi->get_recall_r_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **page_count** | **int**| Maximum number of elements in on call. By default 50.  | [optional] 
 **page_number** | **int**| Page of asked resulset. By default it is the first page.  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_recall_r**
> object put_recall_r(id, response_type, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, negative_response_reason_code=negative_response_reason_code, negative_response_additional_information=negative_response_additional_information, response_comment=response_comment)

Consent on a recall or RRO

Consent on a recall or RRO

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.RecallRApi(swagger_client.ApiClient(configuration))
id = 789 # int | Object internal id.
response_type = 56 # int | Response to the received recall request. If 1, the recall request is accepted and it will generate a positive response to the originating bank. If 0, the recall request is refused. It will generate a negative response to the originating bank. 
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
negative_response_reason_code = 'negative_response_reason_code_example' # str | Reason of negative response type. Required if responseType is false. This field is non case sensitive.  (optional)
negative_response_additional_information = 'negative_response_additional_information_example' # str | Free field to add more informations of a negative response type.  (optional)
response_comment = 'response_comment_example' # str | Free field to add more informations regardless the response type.  (optional)

try:
    # Consent on a recall or RRO
    api_response = api_instance.put_recall_r(id, response_type, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, negative_response_reason_code=negative_response_reason_code, negative_response_additional_information=negative_response_additional_information, response_comment=response_comment)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RecallRApi->put_recall_r: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Object internal id. | 
 **response_type** | **int**| Response to the received recall request. If 1, the recall request is accepted and it will generate a positive response to the originating bank. If 0, the recall request is refused. It will generate a negative response to the originating bank.  | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **negative_response_reason_code** | **str**| Reason of negative response type. Required if responseType is false. This field is non case sensitive.  | [optional] 
 **negative_response_additional_information** | **str**| Free field to add more informations of a negative response type.  | [optional] 
 **response_comment** | **str**| Free field to add more informations regardless the response type.  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

