# coding: utf-8

"""
    Treezor

    Treezor API.  more info [here](https://www.treezor.com).   # noqa: E501

    OpenAPI spec version: 0.1.1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from swagger_client.api_client import ApiClient


class PayoutRefundsApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def get_payout_refunds(self, **kwargs):  # noqa: E501
        """search pay outs refund  # noqa: E501

        Get pay out refund that match search criteria.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_payout_refunds(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str access_signature: Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).
        :param str access_tag: Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).
        :param int access_user_id: Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).
        :param str access_user_ip: Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).
        :param int id: PayoutRefund id
        :param str tag: PayoutRefund tag
        :param str code_status: PayoutRefund Code Status
        :param str information_status: PayoutRefund Information Status
        :param int payout_id: Payout Id
        :param int page_number: The page number
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_payout_refunds_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_payout_refunds_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_payout_refunds_with_http_info(self, **kwargs):  # noqa: E501
        """search pay outs refund  # noqa: E501

        Get pay out refund that match search criteria.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_payout_refunds_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str access_signature: Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).
        :param str access_tag: Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).
        :param int access_user_id: Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).
        :param str access_user_ip: Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).
        :param int id: PayoutRefund id
        :param str tag: PayoutRefund tag
        :param str code_status: PayoutRefund Code Status
        :param str information_status: PayoutRefund Information Status
        :param int payout_id: Payout Id
        :param int page_number: The page number
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_signature', 'access_tag', 'access_user_id', 'access_user_ip', 'id', 'tag', 'code_status', 'information_status', 'payout_id', 'page_number']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_payout_refunds" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_signature' in params:
            query_params.append(('accessSignature', params['access_signature']))  # noqa: E501
        if 'access_tag' in params:
            query_params.append(('accessTag', params['access_tag']))  # noqa: E501
        if 'access_user_id' in params:
            query_params.append(('accessUserId', params['access_user_id']))  # noqa: E501
        if 'access_user_ip' in params:
            query_params.append(('accessUserIp', params['access_user_ip']))  # noqa: E501
        if 'id' in params:
            query_params.append(('id', params['id']))  # noqa: E501
        if 'tag' in params:
            query_params.append(('tag', params['tag']))  # noqa: E501
        if 'code_status' in params:
            query_params.append(('codeStatus', params['code_status']))  # noqa: E501
        if 'information_status' in params:
            query_params.append(('informationStatus', params['information_status']))  # noqa: E501
        if 'payout_id' in params:
            query_params.append(('payoutId', params['payout_id']))  # noqa: E501
        if 'page_number' in params:
            query_params.append(('pageNumber', params['page_number']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['api_key']  # noqa: E501

        return self.api_client.call_api(
            '/payoutRefunds', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='object',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
